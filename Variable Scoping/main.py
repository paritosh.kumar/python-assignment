def num_square(num):
    square = num * num
    # square is local scope
    return square

input_num = 100
# global scope

if input_num > 100:
    result = num_square(5)
    # Consider to be local scope


print(result)
# NameError if input_num is smaller than 100