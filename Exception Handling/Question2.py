
class InvalidNameError(Exception):
  def __init__(self,*args):
    if args:
      self.message = args[0]
    else:
      self.message = None
  
  def __str__(self):
    if self.message:
      return 'InvalidNameError: {0}'.format(self.message)
    else:
      return 'InvalidNameError has been raised.'


class InvalidEmailError(Exception):
  def __init__(self,*args):
    if args:
      self.message = args[0]
    else:
      self.message = None
  
  def __str__(self):
    if self.message:
      return 'InvalidEmailError: {0}'.format(self.message)
    else:
      return 'InvalidEmailError has been raised.'

class InvalidPhoneError(Exception):
  def __init__(self,*args):
    if args:
      self.message = args[0]
    else:
      self.message = None
  
  def __str__(self):
    if self.message:
      return 'InvalidPhoneError: {0}'.format(self.message)
    else:
      return 'InvalidPhoneError has been raised.'

class InvalidAgeError(Exception):
  def __init__(self,*args):
    if args:
      self.message = args[0]
    else:
      self.message = None
  
  def __str__(self):
    if self.message:
      return 'InvalidAgeError: {0}'.format(self.message)
    else:
      return 'InvalidAgeError has been raised.'

persons = []

def add_person(person):
  if not isinstance(person.name,str):
    raise InvalidNameError()
  
  elif not isinstance(person.age,int):
    raise InvalidAgeError()
  
  elif not isinstance(person.email,str):
    raise InvalidEmailError()

  elif not isinstance(person.phone,str) and len(person.phone)!=10:
    raise InvalidPhoneError()

  persons.append(person)