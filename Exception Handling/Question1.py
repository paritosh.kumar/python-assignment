def Question1():
  # ZeroDivisionError
  try:
    print(1/0)
  except ZeroDivisionError:
    print('Zero Division Error')

  
  ob = {
    "key":1
  }

  try:
  # KeyError
    print(ob['key2'])
  except KeyError:
    print('key is not there')

  ar = [1,2]

  # IndexError
  try:
    print(ar[2])
  except IndexError:
    print('Index not in range')


  # SyntaxError
  try:
    open("imaginary.txt")
  except FileNotFoundError:
    print('File not found')

  try:
  # TypeError
    ar.pop('last')
  except TypeError:
    print('Type error')

Question1()