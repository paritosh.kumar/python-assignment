import re

def check_date(inp):
  months = 'January,February,March,April,May,June,July,August,September,October,November,December'.split(',')
  date = inp.split(' ')
  if len(date)!=3:
    return False
  elif date[0].title() not in months:
    return False
  elif not re.fullmatch(r'0[1-9]|[12][0-9]|3[01],',date[1][:-1]):
    return False
  elif not re.fullmatch(r'[0-9]{4}$)',date[-1]):
    return False
  else:
    return True
    # retr
  
  

def validate_credit_number(inp):
  PATTERN = '^([456][0-9]{3})-?([0-9]{4})-?([0-9]{4})-?([0-9]{4})$'
  match = re.match(PATTERN,inp)

  if not match:
    return False
  for i in range(1,len(match)):
    # 2 or more consecutive repeated digits 
    # 22 will return False
    if match[i]==match[i-1]:
      return False

  return True