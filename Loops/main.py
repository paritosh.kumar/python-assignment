def Question1(arr):
  temp = ''
  for x in arr:
    temp+=str(x)
  return temp

def Question2():
  color_list_1 = set(["White", "Black", "Red"])
  color_list_2 = set(["Red", "Green"])
  return color_list_1.difference(color_list_2)