def Question1():
  temp = input('Enter String: ')
  return temp[0:2]+temp[-2:] if len(temp)>=2 else ''

def Question2(arr):
  return max(arr)

def Question3():
  dic1={1:10, 2:20}
  dic2={3:30, 4:40}
  dic3={5:50,6:60}
  dict4={}
  for d in (dic1,dic2,dic3): dict4.update(d)
  return (dict4)