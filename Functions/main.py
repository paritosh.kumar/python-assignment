def Question1():
  num1 = int(input('Enter Number 1'))
  num2 = int(input('Enter Number 2'))
  # GCD
  while num2:
    num1, num2 = num2, num1%num2
  return num1
    
def Question2():
  num1 = int(input('Enter Number 1'))
  num2 = int(input('Enter Number 2'))
  return (num1+num2)**2