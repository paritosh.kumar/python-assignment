def Question1():
  temp = input('Enter String')
  return 'Is'+temp if temp[0:2] == 'Is' else temp

def Question2():
  temp = int(input('Enter Number'))
  return 'Odd' if temp%2 else 'Even'

def Question3():
  return True if input('Enter letter') in list('aeiou') else False