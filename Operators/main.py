import math
import datetime
def Question1(radius):
  return math.pi*radius**2

def Question2(val):
  return val+int(str(val)*2)+int(str(val)*3)

def Question3():
  date1 = list(map(int,input('Enter date 1').split(',')))
  date2 = list(map(int,input('Enter date 2').split(',')))
  d1 = datetime.datetime(date1[0],date1[1],date1[2])
  d2 = datetime.datetime(date2[0],date2[1],date2[2])
  return (d2-d1).days