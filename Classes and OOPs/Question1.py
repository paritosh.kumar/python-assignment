class Country:
  AVG_POPULATION = 1000
  @staticmethod
  def world_avg_population(arr):
    avg = 0
    for x in arr:
      if isinstance(x,int):
        avg+=x
      else:
        raise ValueError('Fill array with integers')
    return avg/len(arr)

  def __init__(self,country_name, country_code):
    if isinstance(country_code,str) and isinstance(country_name,str) and len(country_code) == 3:
      self._country_code = country_code
      self._country_name = country_name
    else:
      raise ValueError('Only string allowed and country code is of length 3')

  def country_short_form(self):
    return self._country_name.capitalize() if len(self._country_name)<2  else self._country_name[:2].capitalize()

  def is_densly_populated(self,population):
    if isinstance(population,int):
      return True if population > AVG_POPULATION else False
    else:
      raise ValueError()

  @property
  def formatted_country_name(self):
    return '{name} ({code})'.format(name=self.country_name, code=self.country_code)
  
  def get_country_name(self): 
    return self._country_name
       
     # function to set value of _age 
  def set_country_name(self, country_name): 
    if isinstance(country_name,str):
      self._country_name = country_name 
    else:
      raise ValueError('Has to be a string')

  # function to delete _age attribute 
  def del_country_name(self): 
    del self._country_name 
     
  country_name = property(get_country_name, set_country_name, del_country_name)

  def get_country_code(self): 
    return self.country_code
       
     # function to set value of _age 
  def set_country_code(self, country_code): 
    if isinstance(country_code,str):
      self.country_code = country_code 
    else:
      raise ValueError('Has to be a string')

  # function to delete _age attribute 
  def del_country_code(self): 
    del self.country_code 
     
  country_code = property(get_country_code, set_country_code, del_country_code)