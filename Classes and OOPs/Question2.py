class Shape:
  def __init__(self,width,height):
    if isinstance(width,int) and isinstance(height,int):
      self._sides = 2
      self._width = width
      self._height = height
    else:
      raise ValueError('Only integers can be provided as inputs')

  def get_sum(self):
    return self._width + self._height

  def get_prod(self):
    return self._width * self._height

  @property
  def width(self):
    return self._width

  @property
  def sides(self):
    return self._sides
  
  @width.setter 
  def width(self,width):
    if isinstance(width,int):
      self._width = width
    else:
      raise ValueError('Only integer accepted')

  @property
  def height(self):
    return self._height

  @height.setter
  def height(self,height):
    if isinstance(height,int):
      self._height = height
    else:
      raise ValueError('Only integer accepted')

class Triangle(Shape):
  def __init__(self,angle1,angle2,angle3,side1,side2,side3):
    self._sides = 3
    self._angle1 = angle1
    self._angle2 = angle2
    self._angle3 = angle3
    self._side1 = side1
    Shape.__init__(self,side2,side3)
    self._side2 = self.width + 1
    self._side3 = self.height + 1

  def get_sum(self):
    return self._side1+self._side2+self._side3
  
  def print_sides(self):
    print(self._side1,self._side2,self._side3)
  
  def get_double_sum(self):
    return self.get_sum()*2


class Quadrilateral(Shape):
  def __init__(self,angle1,angle2,angle3,angle4,side1,side2,side3,side4):
    self._sides = 4
    self._angle1 = angle1
    self._angle2 = angle2
    self._angle3 = angle3
    self._angle4 = angle4
    self._side1 = side1
    Shape.__init__(self,side2,side3)
    self._side2 = self.width + 1
    self._side3 = self.height + 1
    self._side4 = side4

  def get_sum(self):
    return self._side1+self._side2+self._side3+self._side4

  def print_all_angles(self):
    return print(self._angle1,self._angle2,self._angle3,self._angle3)