import random

print(random.random())
print(random.randint(1,20))
print(random.choice([1,2,34,5]))

from Animals import sum
from Animals import product
from Animals import patterns

arr= [1,2,3]
sum.print_avg(arr)
sum.print_sum(arr)

product.print_prod(arr)
product.print_squares(arr)

patterns.print_pattern1()
patterns.print_pattern2()