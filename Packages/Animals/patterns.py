def print_pattern1():
  for i in range(10):
    print('*'*i)

def print_pattern2():
  for i in range(10,-1,-1):
    print('*'*i)